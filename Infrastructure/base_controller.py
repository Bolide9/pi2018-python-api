import abc
from flask import request
from data.controller.database_controller import *


class BaseController(abc.ABC):
    TABLE = None
    COLUMNS = None
    METHODS = None

    @classmethod
    def __check_methods(cls, method):
        if cls.METHODS is not None:
            if method not in cls.METHODS:
                raise 'Not Allowed'

    @classmethod
    def get_all(cls):
        cls.__check_methods('get_all')
        return build_get_all(cls.TABLE)()

    @classmethod
    def get_sorted_list(cls, id):
        cls.__check_methods('get_by_id_sorted')
        return build_get_sorted_list(cls.TABLE, 'friends')(id)

    @classmethod
    def get_by_id(cls, id):
        cls.__check_methods('get_by_id')
        return build_get_by_id(cls.TABLE)(id)

    @classmethod
    def create(cls):
        cls.__check_methods('create')
        return build_create(cls.TABLE, cls.COLUMNS, [request.json[key] for key in cls.COLUMNS])()

    @classmethod
    def delete(cls, id):
        cls.__check_methods('delete')
        return build_delete(cls.TABLE)(id)

    @classmethod
    def replace(cls, id):
        """PUT"""
        cls.__check_methods('replace')
        return build_replace(cls.TABLE, cls.COLUMNS, [request.json[key] for key in cls.COLUMNS])(id)

    @classmethod
    def update(cls, id):
        """PATCH"""
        cls.__check_methods('update')
        return build_update(cls.TABLE, cls.COLUMNS, request)(id)

    @classmethod
    def delete_all(cls, id):
        pass
