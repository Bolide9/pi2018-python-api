from infrastructure.constants import app
from contacts.urls import urls as contacts_url
from friends.urls import urls as friends_url


def init_urls():
    urls = contacts_url + friends_url

    for url in urls:
        app.add_url_rule(url.path, view_func=url.view, methods=url.methods, endpoint=url.endpoint)
