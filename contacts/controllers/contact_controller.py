from flask import jsonify
from infrastructure.base_controller import BaseController
from data.database import query_db


class ContactController(BaseController):
    TABLE = 'contacts'
    COLUMNS = ['first_name', 'last_name', 'email', 'phone']

    @classmethod
    def get_sorted_list(cls):
        def get_all(id):
            items = query_db(
                f'''SELECT C.*, F.created_at FROM Contacts C
                   JOIN Friends F ON F.first_contact_id = C.id
                   WHERE F.second_contact_id = ?
                   UNION
                   SELECT C.*, F.created_at FROM Contacts C
                   JOIN Friends F ON F.second_contact_id = C.id
                   WHERE F.first_contact_id = ?
                   ORDER BY F.created_at DESC''',
                (id, id))

            return jsonify(items)

        return get_all
