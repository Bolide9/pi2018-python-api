from infrastructure.url import Url
from contacts.controllers.contact_controller import ContactController
from infrastructure.methods_name import *

_contacts = 'contacts'

urls = [
    Url(f'/{_contacts}', ContactController.get_all, [GET], f'{_contacts}.get_all'),
    Url(f'/{_contacts}/<int:id>', ContactController.get_by_id, [GET], f'{_contacts}.get_by_id'),
    Url(f'/{_contacts}/<int:id>/friends', ContactController.get_sorted_list, [GET], f'{_contacts}.getFriends'),
    Url(f'/{_contacts}', ContactController.create, [POST], f'{_contacts}.create'),
    Url(f'/{_contacts}/<int:id>', ContactController.replace, [PUT], f'{_contacts}.replace'),
    Url(f'/{_contacts}/<int:id>', ContactController.update, [PATCH], f'{_contacts}.update'),
    Url(f'/{_contacts}/<int:id>', ContactController.delete, [DELETE], f'{_contacts}.delete'),
]
