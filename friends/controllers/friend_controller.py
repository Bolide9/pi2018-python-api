from infrastructure.base_controller import BaseController


class FriendController(BaseController):
    TABLE = 'friends'
    COLUMNS = ['first_contact_id', 'second_contact_id', 'created_at']
    METHODS = ['create', 'delete']
