class Friend:
    def __init__(self, id, first_contact_id, second_contact_id, created_at):
        self.id = id
        self.first_contact_id = first_contact_id
        self.second_contact_id = second_contact_id
        self.created_at = created_at
