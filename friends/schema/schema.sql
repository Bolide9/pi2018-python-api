CREATE TABLE IF NOT EXISTS friends (
	id INTEGER PRIMARY KEY,
	first_contact_id INTEGER NOT NULL,
	second_contact_id INTEGER NOT NULL,
	created_at TEXT,

	FOREIGN KEY(first_contact_id) REFERENCES contacts(id),
	FOREIGN KEY(second_contact_id) REFERENCES contacts(id)
);