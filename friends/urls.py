from infrastructure.url import Url
from friends.controllers.friend_controller import FriendController
from infrastructure.methods_name import *

_friends = 'friends'

urls = [
    Url(f'/{_friends}', FriendController.create, [POST], f'{_friends}.create'),
    Url(f'/{_friends}/<int:id>', FriendController.delete, [DELETE], f'{_friends}.delete'),
]
